<?php

namespace Drupal\bootstrap_datetime_picker\Plugin\WebformElement;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Datetime\DateHelper;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\DateBase;
use Drupal\webform\Utility\WebformArrayHelper;
use Drupal\webform\Utility\WebformDateHelper;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides a 'date and time' element.
 *
 * @WebformElement(
 *   id = "bootstrap_date_time",
 *   api = "https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Render!Element!Date.php/class/Date",
 *   label = @Translation("Bootstrap Date Time"),
 *   description = @Translation("Provides a form element for date and time selection."),
 *   category = @Translation("Date/time elements"),
 * )
 */
class BootstrapDateTime extends DateBase {

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties() {
    $properties = [
      // Date settings.
      'date_date_format' => '',
      'placeholder' => '',
      'date_date_min' => '-5 years',
      'date_date_max' => '+5 years',
      'size' => '',
      'widget_type' => 'datetime2',
      'disabled_hours' => '',
      'use_current' => '',
    ] + parent::defineDefaultProperties();
    return $properties;
  }

  /* ************************************************************************ */

  /**
   * {@inheritdoc}
   */
  public function prepare(array &$element, ?WebformSubmissionInterface $webform_submission = NULL) {
    // Set default date format to HTML date.
    if (!isset($element['#date_date_format'])) {
      $element['#date_date_format'] = $this->getElementProperty($element, 'date_date_format');
    }
    // Set placeholder attribute.
    if (!empty($element['#placeholder'])) {
      $element['#attributes']['placeholder'] = $element['#placeholder'];
    }
    if (isset($element['#widget_type'])) {
      $element['#attributes']['data-bootstrap-date-time'] = $element['#widget_type'];
    }
    else {
      $element['#attributes']['data-bootstrap-date-time'] = 'datetime2';
    }
    if ($this->getElementProperty($element, 'date_date_min')) {
      $element['#min_date'] = $this->getElementProperty($element, 'date_date_min');
    }
    if ($this->getElementProperty($element, 'date_date_max')) {
      $element['#max_date'] = $this->getElementProperty($element, 'date_date_max');
    }
    if ($this->getElementProperty($element, 'date_days')) {
      $all_days = [0, 1, 2, 3, 4, 5, 6];
      $enabled_days = $this->getElementProperty($element, 'date_days');
      $disabled_days = array_diff($all_days, $enabled_days);
      $element['#disable_days'] = array_values($disabled_days);
    }
    if ($this->getElementProperty($element, 'disabled_hours')) {
      $element['#disabled_hours'] = $this->getElementProperty($element, 'disabled_hours');
    }

    // Prepare element after date format has been updated.
    parent::prepare($element, $webform_submission);

    // Set the (input) type attribute to 'date'.
    // @see \Drupal\Core\Render\Element\Date::getInfo
    $element['#attributes']['type'] = 'bootstrap_date_time';

    // Convert date element into textfield with date picker.
    if (!empty($element['#datepicker'])) {
      // Must manually set 'data-drupal-date-format' to trigger date picker.
      // @see \Drupal\Core\Render\Element\Date::processDate
      $element['#attributes']['data-drupal-date-format'] = [$element['#date_date_format']];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setDefaultValue(array &$element) {
    if ($this->hasMultipleValues($element)) {
      $element['#default_value'] = (isset($element['#default_value'])) ? (array) $element['#default_value'] : NULL;
      return;
    }

    // Datelist and Datetime require #default_value to be DrupalDateTime.
    if (in_array($element['#type'], ['datelist', 'datetime'])) {
      if (!empty($element['#default_value']) && is_string($element['#default_value'])) {
        $element['#default_value'] = ($element['#default_value']) ? DrupalDateTime::createFromTimestamp(strtotime($element['#default_value'])) : NULL;
      }
    }
    if ($element['#type'] == 'bootstrap_date_time') {
      $element['#default_value'] = ($element['#default_value']) ? DrupalDateTime::createFromTimestamp(strtotime($element['#default_value'])) : NULL;
    }
  }

  /**
   * Webform element pre validation handler for Date elements.
   */
  public static function preValidateDate(&$element, FormStateInterface $form_state, &$complete_form) {
    // ISSUE #2723159:
    // Datetime form element cannot validate when using a
    // format without seconds.
    // WORKAROUND:
    // Append the second format before the time element is validated.
    //
    // @see \Drupal\Core\Datetime\Element\Datetime::valueCallback
    // @see https://www.drupal.org/node/2723159
    if ($element['#type'] === 'datetime' && $element['#date_time_format'] === 'H:i' && strlen($element['#value']['time']) === 8) {
      $element['#date_time_format'] = 'H:i:s';
    }

    // ISSUE:
    // Date list in composite element is missing the date object.
    //
    // WORKAROUND:
    // Manually set the date object.
    $date_element_types = [
      'datelist' => '\Drupal\Core\Datetime\Element\Datelist',
      'datetime' => '\Drupal\Core\Datetime\Element\Datetime',
    ];

    if (isset($date_element_types[$element['#type']])) {
      $date_class = $date_element_types[$element['#type']];
      $input_exists = FALSE;
      $input = NestedArray::getValue($form_state->getValues(), $element['#parents'], $input_exists);
      if (!isset($input['object'])) {
        // Time picker converts all submitted time values to H:i:s format.
        // @see \Drupal\webform\Element\WebformTime::validateWebformTime
        if (isset($element['#date_time_element']) && $element['#date_time_element'] === 'timepicker') {
          $element['#date_time_format'] = 'H:i:s';
        }
        $input = $date_class::valueCallback($element, $input, $form_state);
        $form_state->setValueForElement($element, $input);
        $element['#value'] = $input;
      }
    }
  }

  /**
   * Webform element validation handler for date elements.
   *
   * Note that #required is validated by _form_validate() already.
   *
   * @see \Drupal\Core\Render\Element\Number::validateNumber
   */
  public static function validateDate(&$element, FormStateInterface $form_state, &$complete_form) {
    $value = $element['#value'];
    $name = empty($element['#title']) ? $element['#parents'][0] : $element['#title'];
    $date_date_format = (!empty($element['#date_date_format'])) ? $element['#date_date_format'] : DateFormat::load('html_date')->getPattern();
    $date_time_format = (!empty($element['#date_time_format'])) ? $element['#date_time_format'] : DateFormat::load('html_time')->getPattern();
    $is_required = $element['#required'];

    // Convert DrupalDateTime array and object to ISO datetime.
    if (is_array($value)) {
      $value = ($value['object']) ? $value['object']->format(DateFormat::load('html_datetime')->getPattern()) : '';
    }
    elseif ($value) {
      if (is_object($value) && get_class($value) == 'Drupal\Core\Datetime\DrupalDateTime') {
        $value = $value->format(DateFormat::load('html_datetime')->getPattern());
      }
      elseif (!$is_required) {
        return;
      }
      else {
        $datetime = WebformDateHelper::createFromFormat($date_date_format, $value);
        if ($datetime === FALSE || static::formatDate($date_date_format, $datetime->getTimestamp()) !== $value) {
          $form_state->setError($element, t('%name must be a valid date.', ['%name' => $name]));
          $value = '';
        }
        else {
          // Clear timestamp to date elements.
          if ($element['#type'] === 'date') {
            $datetime->setTime(0, 0, 0);
            $value = $datetime->format(DateFormat::load('html_date')
              ->getPattern());
          }
          else {
            $value = $datetime->format(DateFormat::load('html_datetime')
              ->getPattern());
          }
        }
      }
    }

    $form_state->setValueForElement($element, $value);
    if ($value === '') {
      return;
    }

    $time = strtotime($value);

    // Ensure that the input is greater than #date_date_min property, if set.
    if (!empty($element['#date_date_min'])) {
      $min = strtotime(static::formatDate('Y-m-d', strtotime($element['#date_date_min'])));
      if ($time < $min) {
        $form_state->setError($element, t('%name must be on or after %min.', [
          '%name' => $name,
          '%min' => static::formatDate($date_date_format, $min),
        ]));
      }
    }

    // Ensure that the input is less than the #date_date_max property, if set.
    if (!empty($element['#date_date_max'])) {
      $max = strtotime(static::formatDate('Y-m-d 23:59:59', strtotime($element['#date_date_max'])));
      if ($time > $max) {
        $form_state->setError($element, t('%name must be on or before %max.', [
          '%name' => $name,
          '%max' => static::formatDate($date_date_format, $max),
        ]));
      }
    }

    // Ensure that the input is greater than the #date_min property, if set.
    if (!empty($element['#date_min'])) {
      $min = strtotime($element['#date_min']);
      if ($time < $min) {
        $form_state->setError($element, t('%name must be on or after %min.', [
          '%name' => $name,
          '%min' => static::formatDate($date_date_format, $min) . ' ' . static::formatDate($date_time_format, $min),
        ]));
      }
    }

    // Ensure that the input is less than the #date_max property, if set.
    if (!empty($element['#date_max'])) {
      $max = strtotime($element['#date_max']);
      if ($time > $max) {
        $form_state->setError($element, t('%name must be on or before %max.', [
          '%name' => $name,
          '%max' => static::formatDate($date_date_format, $max) . ' ' . static::formatDate($date_time_format, $max),
        ]));
      }
    }

    // Ensure that the input is a day of week.
    if (!empty($element['#date_days'])) {
      $days = $element['#date_days'];
      $day = date('w', $time);
      if (!in_array($day, $days)) {
        $form_state->setError($element, t('%name must be a %days.', [
          '%name' => $name,
          '%days' => WebformArrayHelper::toString(array_intersect_key(DateHelper::weekDays(TRUE), array_combine($days, $days)), t('or')),
        ]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getItemFormat(array $element) {
    $format = parent::getItemFormat($element);
    // Drupal's default date fallback includes the time so we need to fallback
    // to the specified or default date only format.
    if ($format === 'fallback') {
      $format = $element['#date_date_format'] ?? $this->getDefaultProperty('date_date_format');
    }
    return $format;
  }

  /* ************************************************************************ */
  // Element configuration methods.
  /* ************************************************************************ */

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['date']['#description'] = $this->t('Datetime element is designed to have sane defaults so any or all can be omitted.') . ' ' .
      $this->t('Both the date and time components are configurable so they can be output as HTML5 datetime elements or not, as desired.');

    $form['date']['date_date_element'] = [
      '#type' => 'select',
      '#title' => $this->t('Date element'),
      '#options' => [
        'datetime' => $this->t('HTML datetime - Use the HTML5 datetime element type.'),
        'datetime-local' => $this->t('HTML datetime input (localized) - Use the HTML5 datetime-local element type.'),
        'date' => $this->t('HTML date input - Use the HTML5 date element type.'),
        'text' => $this->t('Text input - No HTML5 element, use a normal text field.'),
        'none' => $this->t('None - Do not display a date element'),
      ],
    ];

    $form['date']['date_date_element_datetime_warning'] = [
      '#type' => 'webform_message',
      '#message_type' => 'warning',
      '#message_message' => $this->t('HTML5 datetime elements do not gracefully degrade in older browsers and will be displayed as a plain text field without a date or time picker.'),
      '#access' => TRUE,
      '#states' => [
        'visible' => [
          [':input[name="properties[date_date_element]"]' => ['value' => 'datetime']],
          'or',
          [':input[name="properties[date_date_element]"]' => ['value' => 'datetime-local']],
        ],
      ],
    ];

    $form['date']['date_date_element_none_warning'] = [
      '#type' => 'webform_message',
      '#message_type' => 'warning',
      '#message_message' => $this->t('You should consider using a dedicated Time element, instead of this Date/time element, which will prepend the current date to the submitted time.'),
      '#access' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="properties[date_date_element]"]' => ['value' => 'none'],
        ],
      ],
    ];

    $form['date']['date_date_placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Date placeholder'),
      '#description' => $this->t('The placeholder will be shown in the element until the user starts entering a value.'),
      '#states' => [
        'visible' => [
          ':input[name="properties[date_date_element]"]' => ['value' => 'text'],
        ],
      ],
    ];

    $date_format = DateFormat::load('html_date')->getPattern();
    $form['date']['date_date_format'] = [
      '#type' => 'webform_select_other',
      '#required' => TRUE,
      '#title' => $this->t('Date format'),
      '#options' => [
        $date_format => $this->t(
          'HTML date - @format (@date)',
          ['@format' => $date_format, '@date' => static::formatDate($date_format)]
        ),
        'm/d/Y' => $this->t(
          'Short date - @format (@date)',
          ['@format' => 'm/d/Y', '@date' => static::formatDate('m/d/Y')]
        ),
      ],
      '#other__option_label' => $this->t('Custom…'),
      '#other__placeholder' => $this->t('Custom date format…'),
      '#other__description' => $this->t('Enter date format using <a href="http://php.net/manual/en/function.date.php">Date Input Format</a>., Currently this element only supports numeric values for day, month and years e.g. day(d), month(m), year(y/Y), hours(h/H) and minutes(i)'),
      '#attributes' => ['data-webform-states-no-clear' => TRUE],
      '#states' => [
        'visible' => [
          ':input[name="properties[date_date_element]"]' => ['value' => 'text'],
        ],
      ],
    ];

    $form['date']['date_container']['widget_type'] = [
      '#type' => 'select',
      '#options' => [
        'date' => $this->t('Date'),
        'time' => $this->t('Time'),
        'datetime2' => $this->t('Date and Time'),
      ],
      '#title' => $this->t('Widget type'),
    ];

    $form['date']['disabled_hours'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Disabled Hours'),
      '#description' => $this->t('List of hours which are disabled, enter a comma separated list like 1,2,3 in 24 hours format.'),
    ];

    $form['date']['use_current'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use Current Date'),
      '#description' => $this->t('Set whether to use current date as default when field is clicked.'),
    ];

    return $form;
  }

  /**
   * Format custom date.
   *
   * @param string $custom_format
   *   A PHP date format string suitable for input to date().
   * @param int $timestamp
   *   (optional) A UNIX timestamp to format.
   *
   * @return string
   *   Formatted date.
   */
  protected static function formatDate($custom_format, $timestamp = NULL) {
    /** @var \Drupal\Core\Datetime\DateFormatterInterface $date_formatter */
    $date_formatter = \Drupal::service('date.formatter');
    return $date_formatter->format($timestamp ?: \Drupal::time()->getRequestTime(), 'custom', $custom_format);
  }

  /**
   * {@inheritdoc}
   */
  protected function parseInputFormat(array &$element, $property) {
    if (!isset($element[$property])) {
      return;
    }
    elseif (is_array($element[$property])) {
      foreach ($element[$property] as $key => $value) {
        $timestamp = strtotime($value);
        $element[$property][$key] = ($timestamp !== FALSE) ? $this->dateFormatter->format($timestamp, 'html_' . $this->getDateType($element)) : NULL;
      }
    }
    else {
      $timestamp = strtotime($element[$property]);
      $element[$property] = ($timestamp !== FALSE) ? $this->dateFormatter->format($timestamp, 'custom', 'Y-m-d H:i') : NULL;
    }
  }

}
