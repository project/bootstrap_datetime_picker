<?php

namespace Drupal\bootstrap_datetime_picker\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\datetime\Plugin\Field\FieldWidget\DateTimeWidgetBase;

/**
 * Plugin implementation of the BootstrapDateTimeWidget widget.
 *
 * @FieldWidget(
 *   id = "bootstrap_date_time_widget",
 *   label = @Translation("Bootstrap DateTime Picker"),
 *   field_types = {
 *     "datetime"
 *   }
 * )
 */
class BootstrapDateTimeWidget extends DateTimeWidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'wrapper_class' => 'container',
      'date_date_min' => '-5 years',
      'date_date_max' => '+5 years',
      'column_size_class' => 'col-sm-6',
      'date_date_format' => 'd-m-y H:i',
      'disabled_hours' => '',
      'disable_days' => [],
      'exclude_date' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $elements = [];

    $elements['wrapper_class'] = [
      '#type' => 'select',
      '#title' => $this->t('Wrapper Class'),
      '#options' => [
        'container' => $this->t('Container'),
        'fluid-container' => $this->t('Fluid Container'),
      ],
      '#description' => $this->t('Select the wrapper class. Check https://getbootstrap.com/docs/4.3/layout/overview/'),
      '#default_value' => $this->getSetting('wrapper_class'),
    ];

    $elements['column_size_class'] = [
      '#type' => 'select',
      '#title' => $this->t('Column Size'),
      '#description' => $this->t("Select the column size based on bootstrap's grid system. Check https://getbootstrap.com/docs/4.0/layout/grid/"),
      '#options' => [
        'col-sm-1' => 1,
        'col-sm-2' => 2,
        'col-sm-3' => 3,
        'col-sm-4' => 4,
        'col-sm-5' => 5,
        'col-sm-6' => 6,
        'col-sm-7' => 7,
        'col-sm-8' => 8,
        'col-sm-9' => 9,
        'col-sm-10' => 10,
        'col-sm-11' => 11,
        'col-sm-12' => 12,
      ],
      '#default_value' => $this->getSetting('column_size_class'),
    ];

    $elements['date_date_format'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Date Time Format'),
      '#default_value' => $this->getSetting('date_date_format'),
      '#required' => TRUE,
    ];

    $elements['date_date_min'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Date minimum'),
      '#description' => $this->t('Specifies the minimum date.')
      . ' ' . $this->t('Accepts any date in any <a href="https://www.gnu.org/software/tar/manual/html_chapter/tar_7.html#Date-input-formats">GNU Date Input Format</a>. Strings such as today, +2 months, and Dec 9 2004 are all valid.'),
      '#default_value' => $this->getSetting('date_date_min'),
    ];
    $elements['date_date_max'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Date maximum'),
      '#description' => $this->t('Specifies the maximum date.')
      . ' ' . $this->t('Accepts any date in any <a href="https://www.gnu.org/software/tar/manual/html_chapter/tar_7.html#Date-input-formats">GNU Date Input Format</a>. Strings such as today, +2 months, and Dec 9 2004 are all valid.'),
      '#default_value' => $this->getSetting('date_date_max'),
    ];
    $elements['disabled_hours'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Disabled Hours'),
      '#description' => $this->t('List of hours which are disabled, enter a comma separated list like 1,2,3 in 24 hours format.'),
      '#default_value' => $this->getSetting('disabled_hours'),
    ];
    $elements['disable_days'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Disable specific days in week'),
      '#description' => $this->t('Select days which are disabled in calendar, etc. weekends or just Friday'),
      '#options' => [
        '1' => $this->t('Monday'),
        '2' => $this->t('Tuesday'),
        '3' => $this->t('Wednesday'),
        '4' => $this->t('Thursday'),
        '5' => $this->t('Friday'),
        '6' => $this->t('Saturday'),
        '7' => $this->t('Sunday'),
      ],
      '#default_value' => $this->getSetting('disable_days'),
      '#required' => FALSE,
    ];
    $elements['exclude_date'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Disable specific dates from calendar'),
      '#description' => $this->t('Enter days in following format MM/DD/YY e.g. 03/07/2018. Separate multiple dates with comma. This is used for specific dates, if you want to disable all weekends use settings above, not this field.'),
      '#default_value' => $this->getSetting('exclude_date'),
      '#required' => FALSE,
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = $this->t('Wrapper Class: @wrapper_class', ['@wrapper_class' => $this->getSetting('wrapper_class')]);
    $summary[] = $this->t('Column Size: @column_size_class', ['@column_size_class' => $this->getSetting('column_size_class')]);
    $summary[] = $this->t('Date Format: @date_date_format', ['@date_date_format' => $this->getSetting('date_date_format')]);
    $summary[] = $this->t('Date Minimum: @date_date_min', ['@date_date_min' => $this->getSetting('date_date_min')]);
    $summary[] = $this->t('Date Maximum: @date_date_max', ['@date_date_max' => $this->getSetting('date_date_max')]);
    $summary[] = $this->t('Disabled Hours: @disabled_hours', ['@disabled_hours' => $this->getSetting('disabled_hours')]);

    $options = [
      '1' => $this->t('Monday'),
      '2' => $this->t('Tuesday'),
      '3' => $this->t('Wednesday'),
      '4' => $this->t('Thursday'),
      '5' => $this->t('Friday'),
      '6' => $this->t('Saturday'),
      '7' => $this->t('Sunday'),
    ];

    $disabled_days = [];
    foreach ($this->getSetting('disable_days') as $value) {
      if (!empty($value)) {
        $disabled_days[] = $options[$value];
      }
    }

    $disabled_days = implode(',', $disabled_days);

    $summary[] = $this->t('Disabled days: @disabled_days', ['@disabled_days' => !empty($disabled_days) ? $disabled_days : $this->t('None')]);

    $summary[] = $this->t('Disabled dates: @disabled_dates', ['@disabled_dates' => !empty($this->getSetting('exclude_date')) ? $this->getSetting('exclude_date') : $this->t('None')]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    // Identify the type of date and time elements to use.
    switch ($this->getFieldSetting('datetime_type')) {
      case DateTimeItem::DATETIME_TYPE_DATE:
        $field_type = 'date';
        $time_type = 'none';
        $time_format = '';
        break;

      default:
        $field_type = 'datetime';
        $time_type = 'time';
        $time_format = 'H:i:s';
        break;
    }

    // Field type.
    $element['value']['#title'] = $element['#title'];
    $element['value']['#type'] = 'bootstrap_date_time';
    $element['value']['#date_timezone'] = date_default_timezone_get();
    $element['value']['#date_type'] = $field_type;
    $element['value']['#date_time_format'] = $time_format;
    $element['value']['#date_time_element'] = $time_type;
    $element['value']['#required'] = $element['#required'];
    $element['value']['#wrapper_class'] = $this->getSetting('wrapper_class');
    $element['value']['#column_size_class'] = $this->getSetting('column_size_class');
    $element['value']['#min_date'] = $this->getSetting('date_date_min');
    $element['value']['#max_date'] = $this->getSetting('date_date_max');
    $element['value']['#date_date_format'] = $this->getSetting('date_date_format');
    $element['value']['#disabled_hours'] = $this->getSetting('disabled_hours');
    $element['value']['#disable_days'] = $this->getSetting('disable_days');
    $element['value']['#exclude_date'] = $this->getSetting('exclude_date');
    $element['value']['#description'] = $this->getFilteredDescription();
    return $element;
  }

}
