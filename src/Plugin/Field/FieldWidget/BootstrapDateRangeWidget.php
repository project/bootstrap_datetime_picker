<?php

namespace Drupal\bootstrap_datetime_picker\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime_range\Plugin\Field\FieldWidget\DateRangeWidgetBase;

/**
 * Plugin implementation of the BootstrapDateTimeWidget widget.
 *
 * @FieldWidget(
 *   id = "bootstrap_date_range_widget",
 *   label = @Translation("Bootstrap DateRange Picker"),
 *   field_types = {
 *     "daterange"
 *   }
 * )
 */
class BootstrapDateRangeWidget extends DateRangeWidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'date_date_format' => 'd-m-y H:i',
      'date_date_min' => '-5 years',
      'date_date_max' => '+5 years',
      'disabled_hours' => '',
      'disable_days' => [],
      'exclude_date' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $elements = [];

    $elements['date_date_format'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Date Time Format'),
      '#default_value' => $this->getSetting('date_date_format'),
      '#required' => TRUE,
    ];
    $elements['date_date_min'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Date minimum'),
      '#description' => $this->t('Specifies the minimum date.')
      . ' ' . $this->t('Accepts any date in any <a href="https://www.gnu.org/software/tar/manual/html_chapter/tar_7.html#Date-input-formats">GNU Date Input Format</a>. Strings such as today, +2 months, and Dec 9 2004 are all valid.'),
      '#default_value' => $this->getSetting('date_date_min'),
    ];
    $elements['date_date_max'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Date maximum'),
      '#description' => $this->t('Specifies the maximum date.')
      . ' ' . $this->t('Accepts any date in any <a href="https://www.gnu.org/software/tar/manual/html_chapter/tar_7.html#Date-input-formats">GNU Date Input Format</a>. Strings such as today, +2 months, and Dec 9 2004 are all valid.'),
      '#default_value' => $this->getSetting('date_date_max'),
    ];
    $elements['disabled_hours'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Disabled Hours'),
      '#description' => $this->t('List of hours which are disabled, enter a comma separated list like 1,2,3 in 24 hours format.'),
      '#default_value' => $this->getSetting('disabled_hours'),
    ];
    $elements['disable_days'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Disable specific days in week'),
      '#description' => $this->t('Select days which are disabled in calendar, etc. weekends or just Friday'),
      '#options' => [
        '1' => $this->t('Monday'),
        '2' => $this->t('Tuesday'),
        '3' => $this->t('Wednesday'),
        '4' => $this->t('Thursday'),
        '5' => $this->t('Friday'),
        '6' => $this->t('Saturday'),
        '7' => $this->t('Sunday'),
      ],
      '#default_value' => $this->getSetting('disable_days'),
      '#required' => FALSE,
    ];
    $elements['exclude_date'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Disable specific dates from calendar'),
      '#description' => $this->t('Enter days in following format MM/DD/YY e.g. 03/07/2018. Separate multiple dates with comma. This is used for specific dates, if you want to disable all weekends use settings above, not this field.'),
      '#default_value' => $this->getSetting('exclude_date'),
      '#required' => FALSE,
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = $this->t('Wrapper Class: @wrapper_class', ['@wrapper_class' => $this->getSetting('wrapper_class')]);
    $summary[] = $this->t('Column Size: @column_size_class', ['@column_size_class' => $this->getSetting('column_size_class')]);
    $summary[] = $this->t('Date Format: @date_date_format', ['@date_date_format' => $this->getSetting('date_date_format')]);
    $summary[] = $this->t('Date Minimum: @date_date_min', ['@date_date_min' => $this->getSetting('date_date_min')]);
    $summary[] = $this->t('Date Maximum: @date_date_max', ['@date_date_max' => $this->getSetting('date_date_max')]);
    $summary[] = $this->t('Disabled Hours: @disabled_hours', ['@disabled_hours' => $this->getSetting('disabled_hours')]);
    $options = [
      '1' => $this->t('Monday'),
      '2' => $this->t('Tuesday'),
      '3' => $this->t('Wednesday'),
      '4' => $this->t('Thursday'),
      '5' => $this->t('Friday'),
      '6' => $this->t('Saturday'),
      '7' => $this->t('Sunday'),
    ];

    $disabled_days = [];
    foreach ($this->getSetting('disable_days') as $value) {
      if (!empty($value)) {
        $disabled_days[] = $options[$value];
      }
    }

    $disabled_days = implode(',', $disabled_days);

    $summary[] = $this->t('Disabled days: @disabled_days', [
      '@disabled_days' => !empty($disabled_days) ? $disabled_days : $this->t('None'),
    ]);

    $summary[] = $this->t('Disabled dates: @disabled_dates', [
      '@disabled_dates' => !empty($this->getSetting('exclude_date')) ? $this->getSetting('exclude_date') : $this->t('None'),
    ]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    // Field type.
    $element['value']['#title'] = $this->t('Start date');
    $element['value']['#type'] = 'bootstrap_date_time';
    $element['value']['#date_timezone'] = date_default_timezone_get();
    $element['value']['#date_type'] = 'datetime';
    $element['value']['#required'] = $element['#required'];
    $element['value']['#date_date_format'] = $this->getSetting('date_date_format');
    $element['value']['#min_date'] = $this->getSetting('date_date_min');
    $element['value']['#max_date'] = $this->getSetting('date_date_max');
    $element['value']['#disabled_hours'] = $this->getSetting('disabled_hours');
    $element['value']['#disable_days'] = $this->getSetting('disable_days');
    $element['value']['#exclude_date'] = $this->getSetting('exclude_date');

    $element['end_value']['#title'] = $this->t('End date');
    $element['end_value']['#type'] = 'bootstrap_date_time';
    $element['end_value']['#date_timezone'] = date_default_timezone_get();
    $element['end_value']['#date_type'] = 'datetime';
    $element['end_value']['#required'] = $element['#required'];
    $element['end_value']['#date_date_format'] = $this->getSetting('date_date_format');
    $element['end_value']['#min_date'] = $this->getSetting('date_date_min');
    $element['end_value']['#max_date'] = $this->getSetting('date_date_max');
    $element['end_value']['#disabled_hours'] = $this->getSetting('disabled_hours');
    $element['end_value']['#disable_days'] = $this->getSetting('disable_days');
    $element['end_value']['#exclude_date'] = $this->getSetting('exclude_date');

    return $element;
  }

}
