<?php

namespace Drupal\bootstrap_datetime_picker\Element;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Datetime\Element\Datetime;
use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Provides a BootstrapDateTime form element.
 *
 * @FormElement("bootstrap_date_time")
 */
class BootstrapDateTime extends Datetime {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {

    $class = get_class($this);
    $info = [
      '#input' => TRUE,
      '#element_validate' => [
        [$class, 'validateDatetime'],
      ],
      '#process' => [
        [$class, 'processBootstrapDateTime'],
        [$class, 'processAjaxForm'],
      ],
      '#pre_render' => [
        [$class, 'preRenderBootstrapDateTime'],
        [$class, 'preRenderAjaxForm'],
      ],
      '#multiple' => FALSE,
      '#maxlength' => 512,
      '#size' => 25,
      '#theme_wrappers' => ['form_element'],
      '#theme' => 'input__bootstrap_date_time',
    ];
    return $info;

  }

  /**
   * Render element for input.html.twig.
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *   Properties used: #title, #value, #description, #size, #maxlength,
   *   #placeholder, #required, #attributes.
   *
   * @return array
   *   The $element with prepared variables ready for input.html.twig.
   */
  public static function preRenderBootstrapDateTime(array $element) {
    Element::setAttributes($element, ['id', 'name', 'value', 'size']);
    static::setAttributes($element, ['form-date']);
    return $element;
  }

  /**
   * Value Callback for the Element.
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    $element += ['#date_timezone' => date_default_timezone_get()];

    if ($input !== FALSE) {
      try {
        $date = DrupalDateTime::createFromFormat($element['#date_date_format'], $input, $element['#date_timezone']);
        $input = [
          'date'   => $date->format('Y-m-d'),
          'time'   => $date->format('H:i:s'),
          'object' => $date,
        ];
      }
      catch (\Exception $e) {
        $date = NULL;
        $input = [
          'date'   => '',
          'time'   => '',
          'object' => NULL,
        ];
      }
    }
    else {
      $date = $element['#default_value'] ?? NULL;
      if ($date instanceof DrupalDateTime && !$date->hasErrors()) {
        $date->setTimezone(new \DateTimeZone($element['#date_timezone']));
        $input = [
          'date'   => $date->format('Y-m-d'),
          'time'   => $date->format('H:i:s'),
          'object' => $date,
        ];
      }
      else {
        $input = [
          'date'   => '',
          'time'   => '',
          'object' => NULL,
        ];
      }
    }
    return $input;
  }

  /**
   * {@inheritdoc}
   */
  public static function processBootstrapDateTime(&$element, FormStateInterface $form_state, &$complete_form) {
    // Get system regional settings.
    $first_day = \Drupal::config('system.date')->get('first_day');

    // Get disabled days.
    $disabled_days = [];

    // Get active days.
    foreach ($element['#disable_days'] as $value) {
      if ((int) $value >= 1 && (int) $value <= 7) {
        $disabled_days[] = (int) $value === 7 ? 0 : (int) $value;
      }
    }

    if (!isset($element['#date_date_format'])) {
      if ($element['#date_type'] == 'date') {
        $element['#date_date_format'] = DateFormat::load('html_date')->getPattern();
      }
      elseif ($element['#date_type'] == 'time') {
        $element['#date_date_format'] = DateFormat::load('html_time')->getPattern();
      }
      else {
        $element['#date_date_format'] = DateFormat::load('short')->getPattern();
      }
    }

    // Get excluded dates.
    $exclude_date = [];

    if (!empty($element['#exclude_date'])) {
      $exclude_date = explode(",", $element['#exclude_date']);
    }
    foreach ($exclude_date as $index => $edate) {
      $exclude_date[$index] = date($element['#date_date_format'], strtotime($edate));
    }
    // Default settings.
    $settings = [
      'data-first-day' => $first_day,
      'data-disable-days' => Json::encode($disabled_days),
      'data-exclude-date' => Json::encode($exclude_date),
    ];
    if (isset($element['#min_date'])) {
      $settings['data-min-date'] = date($element['#date_date_format'], strtotime($element['#min_date']));
    }
    if (isset($element['#max_date'])) {
      $settings['data-max-date'] = date($element['#date_date_format'], strtotime($element['#max_date']));
    }
    if (isset($element['#disabled_hours'])) {
      $settings['data-disabled-hours'] = '[' . $element['#disabled_hours'] . ']';
    }
    if (isset($element['#use_current'])) {
      $settings['data-use-current'] = $element['#use_current'];
    }

    // Push field type to JS for changing between date only and time fields.
    // Difference between date and date time fields.
    if (isset($element['#date_type'])) {
      $settings['data-bootstrap-date-time'] = $element['#date_type'];
      $element['#date_date_element'] = $element['#date_type'];
    }

    // Handle #ajax event.
    if (isset($element['#ajax']) && !isset($element['#ajax']['event'])) {
      $element['#ajax']['event'] = 'change';
    }

    // Append our attributes to element.
    $element['#attributes'] += $settings;
    $element['#attributes']['class'] = ['form-control', 'add-on'];
    $element['#attributes']['autocomplete'] = 'off';

    // Prefix and Suffix.
    if (!isset($element['#prefix'])) {
      $wrapper_class = $element['#wrapper_class'] ?? '';
      $column_size_class = $element['#column_size_class'] ?? '';
      $element['#prefix'] = "<div class=" . $wrapper_class . ">
      <div class='row'>
          <div class=" . $column_size_class . ">";
    }
    if (!isset($element['#suffix'])) {
      $element['#suffix'] = "</div></div></div>";
    }

    // Attach library.
    $config = \Drupal::config('bootstrap_datetime_picker.settings');
    $cdn = $config->get('use_tempus_dominas_cdn');
    if ($cdn) {
      $complete_form['#attached']['library'][] = 'bootstrap_datetime_picker/datetimepicker-cdn';
    }
    else {
      $complete_form['#attached']['library'][] = 'bootstrap_datetime_picker/datetimepicker';
    }
    $icon_type = $config->get('icon_type');
    $cdn = $config->get('use_cdn');
    $picker_options = [];
    if ($icon_type == 'fontawesome') {
      if ($cdn) {
        $complete_form['#attached']['library'][] = 'bootstrap_datetime_picker/datetimepicker-fa-icons-cdn';
      }
      else {
        $complete_form['#attached']['library'][] = 'bootstrap_datetime_picker/datetimepicker-fa-icons';
      }
      $picker_options['display'] = [
        'icons' => [
          'time' => 'fa-solid fa-clock',
          'date' => 'fa-solid fa-calendar',
          'up' => 'fa-solid fa-arrow-up',
          'down' => 'fa-solid fa-arrow-down',
          'previous' => 'fa-solid fa-chevron-left',
          'next' => 'fa-solid fa-chevron-right',
          'today' => 'fa-solid fa-calendar-check',
          'clear' => 'fa-solid fa-trash',
          'close' => 'fa-solid fa-xmark',
        ],
      ];
    }
    elseif ($icon_type == 'bootstrap_icon') {
      if ($cdn) {
        $complete_form['#attached']['library'][] = 'bootstrap_datetime_picker/datetimepicker-bi-icons-cdn';
      }
      else {
        $complete_form['#attached']['library'][] = 'bootstrap_datetime_picker/datetimepicker-bi-icons';
      }
      $picker_options['display'] = [
        'icons' => [
          'time' => 'bi bi-clock',
          'date' => 'bi bi-calendar',
          'up' => 'bi bi-arrow-up',
          'down' => 'bi bi-arrow-down',
          'previous' => 'bi bi-chevron-left',
          'next' => 'bi bi-chevron-right',
          'today' => 'bi bi-calendar-check',
          'clear' => 'bi bi-trash',
          'close' => 'bi bi-x',
        ],
      ];
    }
    else {
      $picker_options['display'] = [
        'icons' => [
          'time' => $config->get('display_icons_time'),
          'date' => $config->get('display_icons_date'),
          'up' => $config->get('display_icons_up'),
          'down' => $config->get('display_icons_down'),
          'previous' => $config->get('display_icons_previous'),
          'next' => $config->get('display_icons_next'),
          'today' => $config->get('display_icons_today'),
          'clear' => $config->get('display_icons_clear'),
          'close' => $config->get('display_icons_close'),
        ],
      ];
    }

    $language = $config->get('language');
    if ($language != 'en') {
      $complete_form['#attached']['library'][] = 'bootstrap_datetime_picker/datetimepicker-language-' . $language;
      $picker_options['localization']['locale'] = $language;
    }
    $picker_options['display']['sideBySide'] = $config->get('display_sideBySide');
    $picker_options['display']['calendarWeeks'] = $config->get('display_calendarWeeks');
    $picker_options['display']['viewMode'] = $config->get('display_viewMode');
    $picker_options['display']['toolbarPlacement'] = $config->get('display_toolbarPlacement');
    $picker_options['display']['keepOpen'] = $config->get('display_keepOpen');
    $picker_options['display']['buttons'] = [
      'today' => $config->get('display_buttons_today'),
      'clear' => $config->get('display_buttons_clear'),
      'close' => $config->get('display_buttons_close'),
    ];
    $picker_options['display']['components']['calendar'] = $config->get('display_components_calendar');
    $picker_options['display']['components']['date'] = $config->get('display_components_date');
    $picker_options['display']['components']['month'] = $config->get('display_components_month');
    $picker_options['display']['components']['year'] = $config->get('display_components_year');
    $picker_options['display']['components']['decades'] = $config->get('display_components_decades');
    $picker_options['display']['components']['clock'] = $config->get('display_components_clock');
    $picker_options['display']['components']['hours'] = $config->get('display_components_hours');
    $picker_options['display']['components']['minutes'] = $config->get('display_components_minutes');
    $picker_options['display']['components']['seconds'] = $config->get('display_components_seconds');
    $picker_options['display']['inline'] = $config->get('display_inline');
    $picker_options['display']['theme'] = $config->get('display_theme');

    if ($config->get('hourCycle') != 'undefined') {
      $picker_options['localization']['hourCycle'] = $config->get('hourCycle');
    }
    $format = $element['#date_date_format'];
    $patterns = [
      '/d/',
      '/D/',
      '/j/',
      '/l/',
      '/F/',
      '/M/',
      '/m/',
      '/n/',
      '/y/',
      '/Y/',
      '/A/',
      '/g/',
      '/G/',
      '/h/',
      '/H/',
      '/i/',
      '/s/',
    ];
    $replacements = [
      'dd',
      'ddd',
      'd',
      'dddd',
      'MMMM',
      'MMM',
      'MM',
      'M',
      'yy',
      'yyyy',
      'T',
      'h',
      'H',
      'hh',
      'HH',
      'mm',
      'ss',
    ];
    $format = preg_replace($patterns, $replacements, $format);
    $element['#attributes']['data-format'] = $format;
    $picker_options['localization']['format'] = $format;
    $element['#attached']['drupalSettings'] = ['bootstrap_datetime_picker' => $picker_options];
    // Fix the value of the element.
    $format_settings = [];
    $date = !empty($element['#value']['object']) ? $element['#value']['object'] : NULL;
    $date_value = !empty($date) ? $date->format($element['#date_date_format'], $format_settings) : $element['#value']['date'] . ' ' . $element['#value']['time'];
    $element['#value'] = $date_value;

    return $element;
  }

}
