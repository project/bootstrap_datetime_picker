<?php

namespace Drupal\bootstrap_datetime_picker\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Class BootstrapDateTimeSettingsForm.
 *
 * Provides configuration form for Bootstrap Date Time Picker  module.
 *
 * @package Drupal\bootstratp_datetime_picker\Form
 *
 * @ingroup bootstrap_datetime_picker
 */
class BootstrapDateTimeSettingsForm extends ConfigFormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'bootstrap_datetime_picker_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['bootstrap_datetime_picker.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $library_exists = file_exists('libraries/tempus-dominus');
    if (!$library_exists) {
      $this->messenger()->addError('Tempus Dominus library not found please download and place it in libraries folder.');
    }
    $this->configFactory->getEditable('bootstrap_datetime_picker.settings')
      ->set('icon_type', $values['icon_type'])
      ->set('use_cdn', $values['use_cdn'])
      ->set('use_tempus_dominas_cdn', $values['use_tempus_dominas_cdn'])
      ->set('display_icons_time', $values['display_icons_time'])
      ->set('display_icons_date', $values['display_icons_date'])
      ->set('display_icons_up', $values['display_icons_up'])
      ->set('display_icons_down', $values['display_icons_down'])
      ->set('display_icons_previous', $values['display_icons_previous'])
      ->set('display_icons_next', $values['display_icons_next'])
      ->set('display_icons_today', $values['display_icons_today'])
      ->set('display_icons_clear', $values['display_icons_clear'])
      ->set('display_icons_close', $values['display_icons_close'])
      ->set('display_sideBySide', $values['display_sideBySide'])
      ->set('display_calendarWeeks', $values['display_calendarWeeks'])
      ->set('display_viewMode', $values['display_viewMode'])
      ->set('display_toolbarPlacement', $values['display_toolbarPlacement'])
      ->set('display_keepOpen', $values['display_keepOpen'])
      ->set('display_buttons_today', $values['display_buttons_today'])
      ->set('display_buttons_clear', $values['display_buttons_clear'])
      ->set('display_buttons_close', $values['display_buttons_close'])
      ->set('display_components_calendar', $values['display_components_calendar'])
      ->set('display_components_date', $values['display_components_date'])
      ->set('display_components_month', $values['display_components_month'])
      ->set('display_components_year', $values['display_components_year'])
      ->set('display_components_decades', $values['display_components_decades'])
      ->set('display_components_clock', $values['display_components_clock'])
      ->set('display_components_hours', $values['display_components_hours'])
      ->set('display_components_minutes', $values['display_components_minutes'])
      ->set('display_components_seconds', $values['display_components_seconds'])
      ->set('display_inline', $values['display_inline'])
      ->set('display_theme', $values['display_theme'])
      ->set('hourCycle', $values['hourCycle'])
      ->set('language', $values['language'])
      ->save();
    $this->messenger()->addMessage($this->t('Configuration saved successfully.'));
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('bootstrap_datetime_picker.settings');
    $form['icon_type'] = [
      '#type' => 'select',
      '#title' => $this->t("Icon Type"),
      '#default_value' => $config->get('icon_type'),
      '#options' => [
        'fontawesome' => $this->t('Font Awesome Library'),
        'bootstrap_icon' => $this->t('Bootstrap Icons'),
        'none' => $this->t('Use no icons and will use own css to add icons'),
      ],
    ];
    $form['use_cdn'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Use CDN"),
      '#default_value' => $config->get('use_cdn') ?? TRUE,
      '#description' => $this->t('If you are not using CDN then you need to download the fontawesome or bootstrap icon library in the libraries folder of drupal.'),
    ];
    $form['use_tempus_dominas_cdn'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Use CDN for Tempus dominas"),
      '#default_value' => $config->get('use_tempus_dominas_cdn') ?? TRUE,
      '#description' => $this->t('If you are not using CDN then you need to download the tempus dominas in the libraries folder of drupal.'),
    ];
    $url = Url::fromUri('https://getdatepicker.com/6/options/', ['external' => TRUE]);
    $link = Link::fromTextAndUrl('documentation here', $url)->toString();
    $form['help_text'] = [
      '#type' => 'markup',
      '#markup' => 'For knowing more about these configurations check ' . $link,
    ];
    $form['display'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Display'),
    ];
    $form['display']['icons'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Icons'),
      '#states' => ['visible' => [':input[name=icon_type]' => ['value' => 'none']]],
    ];
    $form['display']['icons']['display_icons_time'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Time Icon Classes"),
      '#default_value' => $config->get('display_icons_time'),
      '#states' => ['required' => [':input[name=icon_type]' => ['value' => 'none']]],
    ];
    $form['display']['icons']['display_icons_date'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Date Icon Classes"),
      '#default_value' => $config->get('display_icons_date'),
      '#states' => ['required' => [':input[name=icon_type]' => ['value' => 'none']]],
    ];
    $form['display']['icons']['display_icons_up'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Up Icon Classes"),
      '#default_value' => $config->get('display_icons_up'),
      '#states' => ['required' => [':input[name=icon_type]' => ['value' => 'none']]],
    ];
    $form['display']['icons']['display_icons_down'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Down Icon Classes"),
      '#default_value' => $config->get('display_icons_down'),
      '#states' => ['required' => [':input[name=icon_type]' => ['value' => 'none']]],
    ];
    $form['display']['icons']['display_icons_previous'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Previous Icon Classes"),
      '#default_value' => $config->get('display_icons_previous'),
      '#states' => ['required' => [':input[name=icon_type]' => ['value' => 'none']]],
    ];
    $form['display']['icons']['display_icons_next'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Next Icon Classes"),
      '#default_value' => $config->get('display_icons_next'),
      '#states' => ['required' => [':input[name=icon_type]' => ['value' => 'none']]],
    ];
    $form['display']['icons']['display_icons_today'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Today Icon Classes"),
      '#default_value' => $config->get('display_icons_today'),
      '#states' => ['required' => [':input[name=icon_type]' => ['value' => 'none']]],
    ];
    $form['display']['icons']['display_icons_clear'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Clear Icon Classes"),
      '#default_value' => $config->get('display_icons_clear'),
      '#states' => ['required' => [':input[name=icon_type]' => ['value' => 'none']]],
    ];
    $form['display']['icons']['display_icons_close'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Close Icon Classes"),
      '#default_value' => $config->get('display_icons_close'),
      '#states' => ['required' => [':input[name=icon_type]' => ['value' => 'none']]],
    ];
    $form['display']['display_sideBySide'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Show Side by Side"),
      '#default_value' => $config->get('display_sideBySide'),
      '#description' => $this->t('Defaults to false'),
    ];
    $form['display']['display_calendarWeeks'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Show calendarWeeks"),
      '#default_value' => $config->get('display_calendarWeeks'),
      '#description' => $this->t('Defaults to false'),
    ];
    $form['display']['display_viewMode'] = [
      '#type' => 'select',
      '#title' => $this->t("View Mode"),
      '#options' => [
        'clock' => $this->t('Clock'),
        'calendar' => $this->t('Calendar'),
        'months' => $this->t('Months'),
        'years' => $this->t('Years'),
        'decades' => $this->t('Decades'),
      ],
      '#default_value' => $config->get('display_viewMode'),
    ];
    $form['display']['display_toolbarPlacement'] = [
      '#type' => 'select',
      '#options' => ['top' => 'Top', 'bottom' => 'Bottom'],
      '#title' => $this->t("Toolbar Placement"),
      '#default_value' => $config->get('display_toolbarPlacement'),
    ];
    $form['display']['display_keepOpen'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Keep the picker window open"),
      '#default_value' => $config->get('display_keepOpen'),
    ];
    $form['display']['display_buttons_today'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Show Today button"),
      '#default_value' => $config->get('display_buttons_today'),
    ];
    $form['display']['display_buttons_clear'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Show clear button"),
      '#default_value' => $config->get('display_buttons_clear'),
    ];
    $form['display']['display_buttons_close'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Show close button"),
      '#default_value' => $config->get('display_buttons_close'),
    ];
    $form['display']['display_components_calendar'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Allow calendar view"),
      '#default_value' => $config->get('display_components_calendar'),
    ];
    $form['display']['display_components_date'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Allow Date view"),
      '#default_value' => $config->get('display_components_date'),
    ];
    $form['display']['display_components_month'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Allow month view"),
      '#default_value' => $config->get('display_components_month'),
    ];
    $form['display']['display_components_year'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Allow year view"),
      '#default_value' => $config->get('display_components_year'),
    ];
    $form['display']['display_components_decades'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Allow decade view"),
      '#default_value' => $config->get('display_components_decades'),
    ];
    $form['display']['display_components_clock'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Allow clock view"),
      '#default_value' => $config->get('display_components_clock'),
    ];
    $form['display']['display_components_hours'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Allow hours view"),
      '#default_value' => $config->get('display_components_hours'),
    ];
    $form['display']['display_components_minutes'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Allow minutes view"),
      '#default_value' => $config->get('display_components_minutes'),
    ];
    $form['display']['display_components_seconds'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Allow seconds view"),
      '#default_value' => $config->get('display_components_seconds'),
    ];
    $form['display']['display_inline'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Displays the picker in a inline div instead of a popup."),
      '#default_value' => $config->get('display_inline'),
    ];
    $form['display']['display_theme'] = [
      '#type' => 'select',
      '#title' => $this->t("Datepicker Theme"),
      '#options' => [
        'light' => $this->t('Light'),
        'dark' => $this->t('Dark'),
        'auto' => $this->t('Auto'),
      ],
      '#default_value' => $config->get('display_theme'),
    ];

    $form['localisation'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Localisation'),
    ];
    $form['localisation']['hourCycle'] = [
      '#type' => 'select',
      '#title' => $this->t("Hour Cycle"),
      '#options' => [
        'undefined' => $this->t('Dynamic let browser decide (default)'),
        'h11' => $this->t('12 hour format with 00 am as start'),
        'h12' => $this->t('12 hour format with 12 am as start'),
        'h23' => $this->t('24 hour format with 00 am as start'),
        'h24' => $this->t('24 hour format with 01 am as start'),
      ],
      '#default_value' => $config->get('hourCycle'),
    ];

    $form['localisation']['language'] = [
      '#type' => 'select',
      '#title' => $this->t("Language"),
      '#options' => [
        'en' => $this->t('English (Default)'),
        'ar' => $this->t('Arabic'),
        'ar-sa' => $this->t('Arabic SA'),
        'de' => $this->t('German'),
        'es' => $this->t('Spanish; Castilian'),
        'fi' => $this->t('Finnish'),
        'fr' => $this->t('French'),
        'it' => $this->t('Italian'),
        'nl' => $this->t('Dutch; Flemish'),
        'pl' => $this->t('Polish'),
        'ro' => $this->t('Romanian'),
        'ru' => $this->t('Russian'),
        'sl' => $this->t('Slovenian'),
        'tr' => $this->t('Turkish'),
      ],
      '#default_value' => $config->get('language'),
    ];

    return parent::buildForm($form, $form_state);
  }

}
