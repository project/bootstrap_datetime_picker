/**
 * @file
 */

(function ($, Drupal, once) {
  'use strict';
  Drupal.behaviors.bootstrap_datetime_picker = {
    attach: function (context, settings) {
      once('datePicker','input[data-bootstrap-date-time]', context).forEach(function (value, i) {
        var picker = new tempusDominus.TempusDominus($(value).parent()[0], settings.bootstrap_datetime_picker);
        var input = $(value);
        var options = {useCurrent: 'false', localization: {format: input.data('format')}};
        if (input.data('disable-days')) {
          update_options(options, 'daysOfWeekDisabled', 'restrictions', input.data('disable-days'));
        }
        if (input.data('min-date')) {
          update_options(options, 'minDate', 'restrictions', input.data('min-date'));
        }
        if (input.data('max-date')) {
          update_options(options, 'maxDate', 'restrictions', input.data('max-date'));
        }
        if (input.data('exclude-date')) {
          update_options(options, 'disabledDates', 'restrictions', input.data('exclude-date'));
        }
        if (input.data('disabled-hours')) {
          update_options(options, 'disabledHours', 'restrictions', input.data('disabled-hours'));
        }
        if (input.data('use-current')) {
          update_options(options, 'useCurrent', '', input.data('use-current'));
        }

        if (input.data('bootstrap-date-time') == 'date') {
          options['display'] = {};
          options['display']['components'] = {};
          options['display']['components']['clock'] = false;
          options['display']['components']['hours'] = false;
          options['display']['components']['minutes'] = false;
          options['display']['components']['seconds'] = false;
        }
        else if (input.data('bootstrap-date-time') == 'time') {
          options['display'] = {};
          options['display']['components'] = {};
          options['display']['components']['calendar'] = false;
          options['display']['components']['date'] = false;
          options['display']['components']['month'] = false;
          options['display']['components']['year'] = false;
          options['display']['components']['decades'] = false;
          options['display']['viewMode'] = 'clock';
        }
        picker.updateOptions(options);
      });

      function update_options(options, data, parent, value) {
        if (parent == '') {
          options[data] = value;
        }
        else {
          if (options[parent] == undefined) {
            options[parent] = {};
          }
          options[parent][data] = value;
        }
      }
    },
  };

})(jQuery, Drupal, once);
